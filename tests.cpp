#include <iostream>
#include <gtest/gtest.h>
#include "kwad.h"


using namespace std;

template<typename T>
T add1(T a, T b) {

    return a + b;
}

template<typename P, typename L, typename Z>
auto add2(P x, L y, Z c) -> decltype(c(x, y)) {
    return c(x, y);
}

//l2
template<typename T, typename T2>
auto add1(T *a, T2 *b) {
    return *a + *b;
}

//l2
auto add1(const char *a, const char *b) {
    string s = a;
    return s + b;
}

template<int rozmiar, typename B, B def>
class Wektor {

private:


public:
    B tab[rozmiar];

    wector() {
        int i;
        for (i = 0; i < rozmiar; i++) {
            tab[i] = def;
        };
    }

    const B &operator[](int x) const {
        return tab[x];
    }

    B &operator[](int x) {
        return tab[x];
    }


    void pokazW() {
        int i;
        for (i = 0; i < rozmiar; i++) {
            cout << tab[i] << endl;
        };
    };

    void pokaz(int i) {
        cout << tab[i] << endl;
    };


};



int tests() {

    cout << add1(3, 4) << endl;
    cout << "F " << add1(3.14, 4.5) << endl;
    cout << "S " << add1<string>("a", "la") << endl;
    cout << "S " << add1<string>("o", "la") << endl;
    cout << "L " << add2(2, 4, [](int x, int y) { return x + y; }) << endl;
    cout << "L " << add2("al", "a", [](string x, string y) { return x + y; }) << endl;
    cout << "L " << add2(2.5, 4, [](float x, int y) { return x + y; }) << endl;
    Wektor<5, int, 3> w;
    w[1] = 10;
    cout << w[1] << endl;

    int p = 1;
    int o = 2;
    int *u = &p;
    int *r = &o;
    const char *f = "Af";
    const char *k = "L";
    cout << add1(*u, *r) << endl;
    cout << add1(f, k) << endl;


    try {
        kwad kw(5, 4, 3);
        cout << kw.miejsce(1) << endl;
        cout << kw.miejsce(2) << endl;
        cout << kw.miejsce(3) << endl;
    }
    catch (string wyjatek) {
        cout << wyjatek << endl;
    }



    return 0;

}
